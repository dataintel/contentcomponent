import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BasewidgetContentComponent } from './src/basewidget.component';


export * from  './src/basewidget.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    BasewidgetContentComponent
  ],
  exports: [
    BasewidgetContentComponent
  ]
})
export class ContentModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: ContentModule,
      providers: []
    };
  }
}
